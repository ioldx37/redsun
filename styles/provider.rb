module RedSun

  class RedSunArtProvider < ArtProvider
    def create_bitmap(id,client,size)
      if client == ART_OTHER
        return find_icon_name(id)
      end
      return NULL_BITMAP
    end
    def find_icon_name(str_art_id)
      icon_name = ConfigHelper['art'][str_art_id] || ConfigHelper['art']['ART_NORMAL_FILE']
      return Bitmap.new(File.join(RedSun::Application.path, 'images', icon_name))
    end
  end

end
