#-----------------------------------------------------
#file: debug_plugin.rb
#date: 20121129
#author: LDx
#goal: ruby debugger plugin
#depends: RedSun plugin model, MVC
#-----------------------------------------------------

=begin
 * *Author* LDx
 * *Copyleft* see MIT License
 * *Date* 20121129
 * *Goal* DebugPlugin is a plugin with ruby debug capabilities for RedSun Ruby Editor
 * *Usage*
   Please, complete methods as appropriate
 * *Depends* :
   - +dep+ : see plugger module
=end

require 'rubygems'
require 'git'

module RedSun

  class DebugPlugin < Plugin

    def self.on_activate
      LogHelper.inf "#{self} Activated"
    end

    def self.on_menu
      create_menu("&Debug") do
        add_menu_entry("ID_DBG_DEBUG", "Debug File\tCtrl-Shift-f8") { on_debug_file }
        add_menu_entry("ID_DBG_STEP_OVER", "Step Over\tf8") { on_debug_step_over }
        add_menu_entry("ID_DBG_STEP_INTO", "Step Into\tf7") { on_debug_step_into }
      end
    end

    def self.on_book_tab
      @book = make_debug_book
    end

    def self.on_deactivate
      LogHelper.inf "Thank you to use '#{self}' RedSun plugin!"
    end

    def self.full_name
      "DebugPlugin"
    end

    def self.description
      "RedSun Debug Plugin"
    end

    def self.author
      "Ldx"
    end

    def self.version
      "1.0.0.0"
    end

    private

    def self.make_debug_book
      mb = frame.message_book
      @bzh = Window.new mb, -1
      @grid = make_standard_grid :parent => @bzh, :title => 'status', :rows => 4, :cols => 3, :col_titles => %w[File Status Path]
      @bzh.set_background_colour WHITE
      @grid.set_size(800,100)
      mb.add_book @bzh, 'Debug'
    end

    def self.make_standard_grid options
      grid = Grid.new(options[:parent], -1)
      grid.create_grid(options[:rows],options[:cols])
      grid.set_default_cell_background_colour(WHITE)
      grid.set_row_label_size( 0 )
      grid.set_col_label_size( 18 )
      grid.set_row_label_alignment(ALIGN_CENTRE, ALIGN_CENTRE)
      grid.set_col_size( 0, 0 )
      grid.set_col_label_value( 0, "File" )
      grid.set_col_label_value( 1, "Status" )
      grid.set_col_label_value( 2, "path" )
      grid.set_col_label_alignment(ALIGN_CENTRE, ALIGN_CENTRE)
      grid
    end

    def self.on_debug_file
      LogHelper.inf "on_debug_file"
    end

    def self.on_debug_step_over
      LogHelper.inf "on_debug_step_over"
    end

    def self.on_debug_step_into
      LogHelper.inf "on_debug_step_into"
    end

  end

end
