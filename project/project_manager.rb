module RedSun

  class ProjectManager
  
    NO_ROOT_NAME = 'Noname'
  
    attr_reader :tv, :mru, :projects, :root_name, :root_path
  
    def initialize treeview
      @tv = treeview
      @mru = []
      @projects = {}
    end
    def load_config
      @root_path = ConfigHelper['project']['root_path']
      @root_name = ConfigHelper['project']['root_name']
      tree_view_set_root @root_name
      tree_view_populate
      tree_view_expand
    end
    def add_project project_path
      add_project_impl project_path
    end
    def remove_project project_name
      project = @projects[project_name]
      @projects.delete project
    end
    def on_exit
      save_config
    end
    def get_project_names
      @projects.keys
    end
    def get_full_file_name project_name, short_file_name
      project_file = @projects[project_name][:project_files][short_file_name]
      project_file.full_file_name
    end

    private

    # TODO: a revoir!!
    def save_config
      #    @config.save
    end
    def tree_view_set_root root_name = NO_ROOT_NAME
      @root_name = root_name
      @root_item = @tv.add_root @root_name, 0
    end
    def add_project_impl project_path, project_name
      project_tree_item = @tv.append_item @root_item, project_name, 0
      @projects[project_name] = { :project_name => project_name, :project_tree_item => project_tree_item,
        :project_path => project_path, :project_files => {} }
      project_tree_item
    end
    def add_project_file_impl project_tree_view_item, project_name, short_file_name
      project = @projects[project_name]
      full_file_name = File.norm_join project[:project_path], short_file_name
      file_tree_view_item = @tv.append_item project_tree_view_item, short_file_name, 0
      project_file = ProjectFile.new file_tree_view_item, project_tree_view_item, full_file_name, short_file_name
      @projects[project_name][:project_files][short_file_name] = project_file
      project_file.full_file_name
    end
    def remove_file_from_project_impl project_name, short_file_name
      @projects[project_name][:files].delete short_file_name
    end
    def tree_view_populate
      tv = browse_project_dirs  #tv[full_path] ==> [short file name 1, ...]
      tv.each do | project_path, tv_project_files |
        project_name = File.basename project_path
        project_tree_view_item = add_project_impl project_path, project_name
        tv_project_files.each do | short_file_name |
          add_project_file_impl project_tree_view_item, project_name, short_file_name
        end
      end
    end
    #tv de la forme tv[File.dirname(e)] << File.basename(e), donc
    #tv[path] ==> [nom de fichier court, ...]
    def browse_project_dirs
      return unless @root_path
      dl = DirList.new
      dl.get_tree @root_path
      dl.tv
    end
    def tree_view_expand
      return unless @root_name
      @tv.expand(@root_item)
    end
    def read_file_content file_name_to_load
      file_content = ""
      begin
        f = File.open(file_name_to_load,'r')
        file_content = f.read
        f.close
      rescue
        #log msg here
      end
      file_content
    end
  
  end

end
