#~ require 'menu_helper'

module RedSun

  class MessageBook < AuiNotebook

    attr_reader :frame

    def initialize(frame, flag, point, size,
        style = AUI_NB_DEFAULT_STYLE|
        AUI_NB_TAB_MOVE|
        NO_BORDER|
        AUI_NB_TAB_SPLIT|
        AUI_NB_TAB_FIXED_WIDTH|
        AUI_NB_CLOSE_ON_ACTIVE_TAB|
        AUI_NB_WINDOWLIST_BUTTON|
        NB_NOPAGETHEME|AUI_NB_BOTTOM)
      super(frame, flag, point, size, style)
      @frame = frame
      #create_menu
      setup_events
    end
    
    def setup_events
      evt_enter_window() { | e | on_enter_window(e) }
      evt_auinotebook_page_close(ID_ANY) { | e | on_page_close(e) }
      evt_auinotebook_page_changed(ID_ANY) { | e | on_page_changed(e) }
      evt_auinotebook_tab_middle_up(ID_ANY) { | e | on_tab_middle_up(e) }
    end

    def current_tab
      return get_page(get_selection)
    end

    def on_enter_window event
      set_focus
      @frame.update_focus self
    end

    def on_page_close(event)
      ret = on_save
      return event.veto unless ret
    end

    #middle mouse button has been clicked on the tab
    def on_tab_middle_up(event)
      on_close
    end

    def on_page_changed(event)
#      next_notebook_tab if event.get_id == ID_NOTEBOOK_NEXT
    end

#    def create_menu
#      menu_helper = MenuHelper.new @frame
#      menu_helper.create_menu "MessageBook" do
#        menu_helper.add_menu_entry("ID_MSGBOOK_PREVIOUS", "Previous Tab\tCtrl-Shift-PgUp") { previous_message_tab }
#        menu_helper.add_menu_entry("ID_MSGBOOK_NEXT", "Next Tab\tCtrl-Shift-PgDn") { next_message_tab }
#      end
#    end

    #--book section

    def add_book book, title
      add_page book, title, true
    end

    def get_book sel = get_selection
      return get_page(sel)
    end

    #--
    #--tab navigation section
    #--

    def previous_tab
      pgc = get_page_count
      return if pgc <= 0
      index = get_selection
      index = pgc if index == 0
      set_selection index - 1
      get_page index - 1
    end

    def next_tab
      pgc = get_page_count
      return if pgc <= 0
      index = get_selection
      index = -1 if index == pgc - 1
      set_selection index + 1
      get_page index + 1
    end

    def get_books
      books = []
      0.upto(get_page_count - 1) { |i| books << get_book(i) }
      books
    end

  end

end
