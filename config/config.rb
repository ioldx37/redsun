
require 'singleton'
require 'yaml'


class ConfigHelper

  include Singleton
  private_class_method :new

  #pour recuperer l'instance
  def self.instance
    return @@instance if defined? @@instance
    @@instance = new
  end

  def self.load(config_file_name)
    @config_file_name = config_file_name
    return true if defined?(@cfg)
    load_config
    @cfg['app_dir'] = File.dirname(@config_file_name)
  end

  def self.save
    save_config
  end

  def self.cfg
    @cfg
  end

  def self.[](key)
    @cfg[key]
  end

  private

  def self.load_config
    begin
      @cfg = YAML::load(File.open(@config_file_name))
    rescue
      File.open(@config_file_name, 'r')
      @cfg = YAML::load(File.open(@config_file_name))
    end
  end

  def self.save_config
    File.open(@config_file_name, 'w' ) do |out|
      YAML.dump(@cfg,out)
    end
  end

end


class BaseConfig

  def load(config_file_name)
    @config_file_name = config_file_name
    return true if defined?(@cfg)
    load_config
  end

  def save
    save_config
  end

  def cfg
    @cfg
  end

  def [](key)
    @cfg[key]
  end

  private

  def load_config
    begin
      @cfg = YAML::load(File.open(@config_file_name))
    rescue
      File.open(@config_file_name, 'r')
      @cfg = YAML::load(File.open(@config_file_name))
    end
  end

  def save_config
    File.open(@config_file_name, 'w' ) do |out|
      YAML.dump(@cfg,out)
    end
  end

end

