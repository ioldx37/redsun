# To change this template, choose Tools | Templates
# and open the template in the editor.

$:.unshift File.join(File.dirname(__FILE__),'..','project')

require 'test/unit'
require 'project_helper'
require 'pp'

include RedSun

class TestCompositeProjectHelper < Test::Unit::TestCase
  def test_project_creation_and_project_names
    pm = ProjectManager.new 'pm'
    redsun = Project.new 'redsun'
    appli = ProjectFile.new 'appli', 'path_to_appli'
    frame = ProjectFile.new 'frame', 'path_to_frame'
    redsun.add_children appli, frame
    gedlab = Project.new 'gedlab'
    ctrlr = ProjectFile.new 'ctrlr', 'path_to_ctrlr'
    consl = ProjectFile.new 'consl', 'path_to_consl'
    gedlab.add_children ctrlr, consl
    pm.add_children redsun, gedlab

    ph = ProjectHelper.new pm
    ph.rename('redsun','rodsen')
    rodsen = ph.find('rodsen')
    assert_equal('rodsen', rodsen.name)
    appli = ph.find('appli')
    assert_equal('appli', appli.name)
    appli = ph.find('frame')
    assert_equal('frame', frame.name)
  end
end
