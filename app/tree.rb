
include Wx

module RedSun

  class TreeDirectory < GenericDirCtrl

    def initialize frame
      super(frame, -1, ConfigHelper['project']['root_path'],DEFAULT_POSITION,
        Size.new(460, 250),SIMPLE_BORDER|DIRCTRL_EDIT_LABELS,
        "Ruby files (.rb .rbw)|.rb .rbw|Yaml files (.yml)",1)
    end

    def setup_events
      evt_enter_window() { | e | on_enter_window(e) }
      evt_leave_window() { | e | on_leave_window(e) }
      evt_mousewheel() { | e | on_mouse_wheel(e) }
    end

    def on_enter_window event
      o = event.get_event_object
      LogHelper.inf "TreeDirectory: on_enter_window #{o.class}" if event.entering
      set_focus
    end

    def on_leave_window event
      o = event.get_event_object
      LogHelper.inf "TreeDirectory: on_leave_window #{o.class}" if event.leaving
    end

    def on_mouse_wheel event
      o = event.get_event_object
      LogHelper.inf "TreeDirectory: on_mouse_wheel #{o.class}"
      set_focus
    end

  end

end