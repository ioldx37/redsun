
class GitLib

  def self.open path
    @git = Git.open(path)
  end

  def self.status
      status = @git.status
      last_commit = @git.log.first
      deleted = status.deleted.collect { |e| e[0] }
      added = status.added.collect { |e| e[0] }
      changed = status.changed.collect { |e| e[0] }
      {
        :author => last_commit.author.name,
        :date => last_commit.author.date,
        :email => last_commit.author.email,
        :otish => last_commit.objectish,
        :message => last_commit.message,
        :deleted => deleted,
        :added => added,
        :changed => changed
      }
  end

end


#      brch = g.branch
#      lg = g.log
#      frst = lg.first
#      snce = lg.since Date.today
#      sha1 = lg[0].sha
#      sha2 = lg[1].sha
#      betw = snce.between sha1, sha2
#      last_commit = g.log.first
#      msg = last_commit.message
#      date = last_commit.date
#      auth = last_commit.author
#      LogHelper.inf "(#{auth.name})#{date} #{msg}"

#      msg = first.message
#      added = first.added.collect { | c | c.filename }
#      added = g.status.added
#      changed = g.status.changed
#      deleted = g.status.deleted
#      LogHelper.inf "Commit message (#{msg})"
#      LogHelper.inf "Added (#{added.size})"
#      added.each   { | arr | LogHelper.inf arr[0] }
#      LogHelper.inf "Changed (#{changed.size})"
#      changed.each { | arr | LogHelper.inf arr[0] }
#      LogHelper.inf "Deleted (#{deleted.size})"
#      deleted.each { | arr | LogHelper.inf arr[0] }



#      tgs = lib.tags
#      ccdd = lib.commit_data last_commit.sha
#      coc = lib.object_contents last_commit.sha
#      df = lib.diff_files
#      lc = lib.object_type last_commit.sha
#      m = @git.log.since "2 weeks ago"
#      f1 = status['RedSun.rb']
#      user = @git.config('user.name')
#      lib = @git.lib
