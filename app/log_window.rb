
module RedSun

  class LogWindow < TextCtrl

    def initialize(frame,text="",id=ID_ANY,point=Point.new(0, 0),size=Size.new(150, 90),style=NO_BORDER|TE_MULTILINE)
      super(frame,id,text,point,size,style)
    end

    def trace(message)
      append_text message
    end

  end

end

