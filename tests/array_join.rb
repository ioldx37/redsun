# To change this template, choose Tools | Templates
# and open the template in the editor.

require 'test/unit' 

class TestArrayJoin < Test::Unit::TestCase
  def test_array_join
    a = [[1,2],[3,4],[5,6]]
    b = [[7,8],[9,1],[2,3]]
    c = a + b
    assert_equal([[1,2],[3,4],[5,6]], a)
    assert_equal([[7,8],[9,1],[2,3]], b)
    assert_equal([[1,2],[3,4],[5,6],[7,8],[9,1],[2,3]], c)
  end

    def test_array_range
      a = [].collect {0.upto(4) { |i| ("index #{i}") }}
      assert_equal([0,1,2,3,4], a)
    end
end
