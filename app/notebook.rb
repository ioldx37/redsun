#~ require 'menu_helper'

module RedSun

  class MainNoteBook < AuiNotebook

    attr_reader :frame

    def initialize(frame, flag, point, size,
        style = AUI_NB_DEFAULT_STYLE|
        AUI_NB_TAB_MOVE|
        NO_BORDER|
        AUI_NB_TAB_SPLIT|
        AUI_NB_TAB_FIXED_WIDTH|
        AUI_NB_CLOSE_ON_ACTIVE_TAB|
        AUI_NB_WINDOWLIST_BUTTON|
        NB_NOPAGETHEME|
        NB_MULTILINE)
      super(frame, flag, point, size, style)
      @frame = frame
      @files_infos = nil
      #create_menu
      setup_events
    end

    def setup_events
      evt_enter_window() { | e | on_enter_window(e) }
      evt_auinotebook_page_close(ID_ANY) { | e | on_page_close(e) }
      evt_auinotebook_page_changed(ID_ANY) { | e | on_page_changed(e) }
      evt_auinotebook_tab_middle_up(ID_ANY) { | e | on_tab_middle_up(e) }
    end
    
    def on_page_close(event)
      ret = on_save
      return event.veto unless ret
    end

    #middle mouse button has been clicked on the tab
    def on_tab_middle_up event
      editor = get_client_data
      i = get_page_index(editor)
      set_selection i
    end

    def on_page_changed(event)
      puts "@frame.on_page_changed"
    end

    def on_enter_window event
      set_focus
      @frame.update_focus self
    end

    def previous_tab
      pgc = get_page_count
      return if pgc <= 0
      index = get_selection
      index = pgc if index == 0
      set_selection index - 1
      editor = get_page(index -1)
      @frame.update_title editor.file.full_path
      @frame.update_tree editor.file.full_path
    end

    def next_tab
      pgc = get_page_count
      return if pgc <= 0
      index = get_selection
      index = -1 if index == pgc - 1
      set_selection index + 1
      editor = get_page(index + 1)
      @frame.update_title editor.file.full_path
      @frame.update_tree editor.file.full_path
    end

    #-- save file section
    
    def on_save_all
      save_all
    end

    def on_save_as
      save_as
    end

    def on_save
      save
    end

    #-- exit / close section

    def on_exit
      ret = true
      modified = get_modified
      if modified.size > 0
        dlg = ConfirmDialog.new @frame, modified
        ret = (dlg.show_modal == ID_OK)
      end
      ret
    end

    def on_close
      editor = current_editor
      if editor.modified?
        dlg = MessageDialog.new(@frame,
          ConfigHelper['file']['save']['text'],
          ConfigHelper['file']['save']['title'],
          YES_NO|CANCEL)
        case dlg.show_modal
        when ID_YES
          close
          add_editor if get_page_count <= 0
        when ID_NO
          discard
        end
      else
        discard
      end
      add_editor if get_page_count <= 0
    end

    def on_close_all
      modified = get_modified
      if modified.size <= 0
        close_all
      else
        dlg = ConfirmDialog.new @frame, modified
        if dlg.show_modal == ID_OK
          add_editor
        end
        add_editor
      end
    end

    #--editor section

    def add_editor file_path = nil
      return if check_already_open file_path
      editor = Editor.new self, file_path
      editor.load if file_path
      add_page editor, editor.file.base, true, editor.file.icon
      @frame.update_title editor.file.full_path
      @frame.update_tree editor.file.full_path
      set_client_data editor
      current_editor.set_focus
      editor
    end

    def find_editor sel
      return get_page(sel)
    end

    def current_editor
      return get_page(get_selection)
    end

    def current_editor_untitled? sel = get_selection
      editor = find_editor sel
      return (editor.file.base == ConfigHelper['special']['untitled'])
    end

    def get_files_infos
      max, files = get_page_count, {}
      return false if max <= 0
      0.upto(max - 1) do |index_editor|
        editor = find_editor(index_editor)
        files[editor.file.base]= editor.file
      end
      files
    end

    def get_modified
      max, editors = get_page_count, []
      return editors if max <= 0
      0.upto(max - 1) do |index_editor|
        editor = find_editor(index_editor)
        editors << editor if editor.modified?
      end
      editors
    end

    def find_editors
      each_page
    end

    def open_selected_popup_file file_base_name
      full_file_path = files_infos[file_base_name].full_path
      add_editor(full_file_path)
      LogHelper.inf "#{file_base_name} selected"
    end

    def set_files_infos files_infos
      @files_infos = files_infos
    end

    def comment
      editor = current_editor
      editor.comment if editor
    end

    def popup_file_list
      return if get_page_count <= 0
      files_infos = get_files_infos
      set_files_infos files_infos
      string_files_names = files_infos.keys.join(' ')
      current_editor.popup_window_list(string_files_names)
    end

    def refresh_tab_name editor
      is_modified = editor.modified?
      star = is_modified ? "*" : ""
      set_page_text get_selection, "#{star}#{editor.file.base}"
      @frame.update_title "#{star}#{editor.file.full_path}"
      @frame.update_tree editor.file.full_path
    end

    def change_file_icon editor = current_editor
      set_page_bitmap get_selection, editor.file.icon
    end

    #-- theme section

    def change_theme name
      find_editors.each do |e|
        ThemeHelper.apply_theme e,name
      end
    end

    #-- bookmark section
    
    def on_bookmark_add
      current_editor.bookmark_add
    end

    def on_bookmark_remove
      current_editor.bookmark_remove
    end

    def on_bookmark_next
      current_editor.bookmark_next
    end

    def on_bookmark_previous
      current_editor.bookmark_previous
    end

    #-- find / replace file section

    def on_find
      current_editor.show_find_dialog
    end

    def on_find_selection direction
      current_editor.find_selection direction
    end

    def on_find_selection_again direction
      current_editor.find_selection_again direction
    end

    def on_replace
      current_editor.show_find_replace
    end

    #-- private members section

    private

    #-- close save private section

    def close_all_files
      max = get_page_count
      return if max <= 0
      max.downto(0) do |n|
        set_selection n
        save_file if editor.get_modify
        delete_page n
      end
    end

    def save editor = current_editor
      return true unless editor
      return save_as if editor.get_modify && current_editor_untitled?
      return save_file(editor)
    end

    def save_as editor = current_editor
      return true unless editor
      pth = editor.file.path
      dlg = get_save_file_dialog("Save file as...", pth, ConfigHelper['file']['wildcard'])
      return false unless dlg.show_modal == ID_OK
      return save_file_as(dlg.get_path)
    end

    def close
      ret = save
      delete_page get_selection
      ret
    end

    def discard
      delete_page get_selection
    end

    def close_all
      ret = true
      max = get_page_count
      return ret if max <= 0
      max.downto(0) do |n|
        set_selection n
        ret = close
      end
      ret
    end

    def check_already_open(file_path)
      ret = false
      0.upto(get_page_count-1) do |index_editor|
        editor = find_editor index_editor
        if editor.file.full_path == file_path
          set_selection index_editor
          ret = true
        end
      end
      return ret
    end

    def get_save_file_dialog title, file_name, wildcard
      dlg = FileDialog.new(self, title, Dir.getwd(), file_name, wildcard, SAVE)
      dlg.set_filter_index 2
      dlg
    end

    def save_file editor = current_editor
      return editor.save if editor.file.base != ConfigHelper['special']['untitled']
      return save_as(editor)
    end

    def save_file_as path, editor = current_editor
      editor.file.rename path
      ret = true
      begin
        editor.save path
        change_file_icon editor
        refresh_tab_name editor
      rescue
        ret = false
      end
      ret
    end

    def save_all
      0.upto(get_page_count-1) do |index_editor|
        set_selection index_editor
        editor = current_editor
        save_file editor
        refresh_tab_name editor
      end
    end

  end

end