module RedSun

  class MenuHelper

    def initialize frame
      @frame = frame
    end
    
    def create_menu name, &block
      @menu = Menu.new
      yield
      @frame.get_menu_bar.append(@menu, name)
    end

    def add_menu_entry str_id, str_help, &block
      @frame.insert_menu_id str_id
      id = eval("RedSunFrame::#{str_id}")
      @menu.append(id,str_help)
      @frame.evt_menu(id) do yield end
     end

  end

end
