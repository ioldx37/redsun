module RedSun

  class ProjectFile

    include Observable

    attr_reader :file_tree_view_item, :project_tree_view_item, :full_file_name, :short_file_name

    def initialize project_manager, file_tree_view_item,
      project_tree_view_item, full_file_name, short_file_name
      @project_manager = project_manager
      @file_tree_view_item = file_tree_view_item
      @project_tree_view_item = project_tree_view_item
      @full_file_name = full_file_name
      @short_file_name = short_file_name
    end

  end

end
