module RedSun

  # The class that actually handles the dropped files; it keeps a
  # reference to the ListBox, and appends items as they are added
  class ListFileDropTarget < Wx::FileDropTarget

    def initialize(parent, list = [])
      super()
      @parent = parent
      @list = list
    end
    # This method is overridden to specify what happens when a file is
    # dropped
    def on_drop_files(x, y, files)
      files.each { | file | @list << file }
      @parent.on_create_drag_n_drop_editor(files)
      true # currently need to return boolean from this method
    end

  end

end


