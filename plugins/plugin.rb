class String

  def underscore
    gsub(/::/, '/').
      gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
      gsub(/([a-z\d])([A-Z])/,'\1_\2').
      tr("-", "_").
      downcase
  end

  def camelize
    split(/[^a-z0-9]/i).map{|w| w.capitalize}.join
  end

end


module RedSun

  class Plugin

    def self.on_plug
      LogHelper.inf "ok, I am #{self} and I'm plugged!! (Please, override me)"
    end

    def self.on_unplug
      LogHelper.inf "ok, I was #{self} and I'm UNplugged!! (Please, override me)"
    end

    def self.on_activate
      LogHelper.inf "ok, I am #{self} and I'm Activated... (Please, override me)"
    end

    def self.on_deactivate
      LogHelper.inf "ok, I am #{self} and I'm Deactivated. (Please, override me)"
    end

    def self.on_menu
      LogHelper.inf "I give #{self} a new menu entry. (Please, override me)"
    end

    def self.full_name
      LogHelper.inf "ok, I am #{self} and I'm sure I have a full name. (Please, override me)"
    end

    def self.description
      LogHelper.inf "ok, I am #{self} and I'm sure I have a description. (Please, override me)"
    end

    def self.author
      LogHelper.inf "ok, I am #{self} and I'm sure my creator have a name. (Please, override me)"
    end

    def self.version
      LogHelper.inf "ok, I am #{self} and my version tag is known. (Please, override me)"
    end

    def self.application
      RedSun::Application.app
    end

    def self.frame
      application.frame
    end

    def self.notebook
      frame.special_notebook
    end

    def self.add_menu_entry str_id, str_help, &block
      frame.insert_menu_id str_id
      id = eval("RedSunFrame::#{str_id}")
      @menu.append(id,str_help)
      frame.evt_menu(id) do yield end
    end

    def self.create_menu name, &block
      @menu = Menu.new
      yield
      mb = frame.menu_bar
      mb.append(@menu, name)
    end

  end

end
