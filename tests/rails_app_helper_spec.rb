# To change this template, choose Tools | Templates
# and open the template in the editor.

require File.join(File.dirname(__FILE__), '../plugins/relative/lib/rails_app_helper')

describe 'RailsAppHelper rspec test suite' do
  before(:each) do
    @rails_app_helper = RedSun::RailsAppHelper.new('C:/temp/ged_support/www/gedlab/app/controllers/collaborateur_controller.rb')
  end

  it "%w[..] must be finally an array of strings" do
    ret = %w[public images javascripts stylesheets]
    ret.should == ["public", "images", "javascripts", "stylesheets"]
  end

  it "path given is is a rails application" do
    ret = @rails_app_helper.is_rails_app?
    ret.should == true
  end
end

