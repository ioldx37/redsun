
module RedSun
  
  class TextFinder

    def initialize editor
      @editor = editor
    end
    
    def find_selection direction, selected = nil
      @selected = selected 
      return if @selected.size <= 0
      @find_data = FindReplaceData.new if @find_data.nil?
      flags = @find_data.get_flags
      @find_data.set_find_string @selected
      if @selected
        case direction
        when :next
          @find_data.set_flags(flags | FR_DOWN)
          res = find
          unless res
            @editor.goto_pos 0
            find_selection direction, @selected
          end
        when :previous
          @find_data.set_flags(flags ^ FR_DOWN)
          res = find
          @find_data.set_flags(flags)
          unless res
            @editor.goto_pos @editor.get_text_length
            find_selection direction, @selected
          end
        end
      end
    end

    def show_find_dialog
      @find_data = FindReplaceData.new if @find_data.nil?
      flags = @find_data.get_flags
      @find_data.set_flags(flags | FR_DOWN)
      dlg_find = FindReplaceDialog.new(@editor,@find_data,"Find")
      dlg_find.show(true)
    end

    def show_find_replace
      @find_data = FindReplaceData.new if @find_data.nil?
      flags = @find_data.get_flags
      @find_data.set_flags(flags | FR_DOWN)
      dlg_find = FindReplaceDialog.new(@editor,@find_data,"Replace",FR_REPLACEDIALOG)
      dlg_find.show(true)
    end

    def handle_find_next
      find
    end

    def handle_find_prev
      flags = @find_data.get_flags
      if flags.has_style? FR_DOWN
        @find_data.set_flags(flags ^ FR_DOWN)
      else
        @find_data.set_flags(flags | FR_DOWN)
      end
      find
      @find_data.set_flags(flags)
    end

    def find_command
      find
    end
    
    def find_next_command
      find
    end

    def find_replace_command
      if (@editor.get_selection_end - @editor.get_selection_start > 0 && 
          @editor.get_selected_text == @find_data.get_find_string)
        @editor.replace_selection @find_data.get_replace_string
        ret = find
      else
        ret = find
      end
      ret
    end

    def find_replace_all_command
      if @editor.get_selection_end - @editor.get_selection_start > 0
        cur_pos = [@editor.get_selection_start,@editor.get_selection_end]
      else
        cur_pos = @editor.get_current_pos
      end
      flags = @find_data.get_flags
      @find_data.set_flags(flags |= FR_DOWN)
      nmbr_fnd = 0
      ret = true
      loop do
        ret = find
        if ret
          @editor.replace_selection(@find_data.get_replace_string)
          nmbr_fnd += 1
        else
          break
        end
      end
      if cur_pos.class == Array
        @editor.set_selection(*cur_pos)
        @editor.goto_pos(cur_pos[1])
      else
        @editor.goto_pos(cur_pos)
      end
      message_box("Replaced #{nmbr_fnd} instances of #{@find_data.get_find_string}","Replace",ID_OK|ICON_INFORMATION)
    end

    def find
      flags = @find_data.get_flags
      if flags == FR_DOWN
        minPos = @editor.get_selection_end
        maxPos = @editor.get_text_length
      else
        minPos = @editor.get_selection_start
        maxPos = 0
      end
      i = @editor.find_text(minPos,maxPos,@find_data.get_find_string,flags)
      if i == STC_INVALID_POSITION
        return false
      end
      @editor.goto_pos i
      @editor.set_selection_start i
      @editor.set_selection_end i+@find_data.get_find_string.length
      return true
    end

  end

end