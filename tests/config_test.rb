# To change this template, choose Tools | Templates
# and open the template in the editor.

APPLICATION_PATH = File.join(File.dirname(__FILE__),'..')
$:.unshift File.join(APPLICATION_PATH,'config')

require 'test/unit'
require 'config'

class ConfigTest < Test::Unit::TestCase
  #pour charger une config avec la classe ConfigHelper
  def test_load_config_by_the_config_singleton
    config_file_path = File.join(APPLICATION_PATH,'config.yml')
    ConfigHelper.load(config_file_path)
    assert_equal("RedSun", ConfigHelper['app']['title'])
  end
  #pour charger une config avec la classe Config
  def test_load_config_by_a_config_class
    config_file_path = File.join(APPLICATION_PATH,'config.yml')
    config = SimpleConfig.new
    config.load(config_file_path)
    assert_equal("RedSun", config['app']['title'])
  end
  #pour charger plusieurs configs avec la classe ConfigCollection
  def test_load_config_by_a_config_collection
    cc = ConfigCollection.new
    cc.load('config_2', File.join(File.dirname(__FILE__),'config_2.yml'))
    cc.select('config_2')
    p cc.list
    p cc.current
    p cc.cfg['app2']['title']
    p cc.cfg['app2']['about']
#    assert_equal("RedSun3", cc.cfg['app']['title'])

#    cc.load('config_2', File.join(File.dirname(__FILE__),'config_2.yml'))
#    assert_equal('config_2',cc.current)
#    assert_equal("RedSun2", cc.cfg['app']['title'])

  end
end
