module RedSun

  class RelativeFileConfig < RelativeFile

    def initialize(frame, file_attr, cfg, pluralize = false)
      super(frame, file_attr, cfg, pluralize)
    end
    def get_relative_file
      if @rah.is_rails_app?
        file_key_list = @cfg.keys
        spli = @rah.splitted.collect {|e| e.split('.')[0]}
        key = (spli & file_key_list).to_s
        key = @cfg["default"] if key.empty?
        value = @cfg[key]
        method_name = "open_file_#{value}"
        self.method(method_name.intern).call
        @fullpathfile
      end
    end
    #
    #--implementation
    #
    #
    #-- -- config files : boot -> databases -> environment -> routes ..
    #
    def open_file_database
      @fullpathfile = File.join(@rah.base_config,"database.yml")
    end
    def open_file_environment
      @fullpathfile = File.join(@rah.base_config,"environment.rb")
    end
    def open_file_routes
      @fullpathfile = File.join(@rah.base_config,"routes.rb")
    end
    def open_file_boot
      @fullpathfile = File.join(@rah.base_config,"boot.rb")
    end
    #open boot by default, or select other file from open files list
    def open_file_default
      if !@rah.splitted.include?("config")
        open_file_boot
      else
        @fullpathfile = nil
        @frame.on_popup_file_list  
      end
    end

  end

end