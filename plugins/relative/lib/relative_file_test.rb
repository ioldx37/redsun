module RedSun

  class RelativeFileTest < RelativeFile

    def initialize(frame, file_attr, cfg, pluralize = false)
      super(frame, file_attr, cfg, pluralize)
    end
    def get_relative_file
      #flags to know the type of file currently edited
      #unit testing files : unit -> fixture -> func -> unit ..
      open_fixt_file if @rah.splitted.include?("unit")               #unit -> fixture
      open_unit_file if @rah.splitted.include?("functional")         #functional -> open controller
      open_func_file if @rah.splitted.include?("fixtures")           #fixture -> functional
      #--standard files : mdls -> unit ; view -> unit ; helper -> unit ; ctrl -> func
      open_unit_from_modl if @rah.splitted.include?("models")        #model -> unit
      open_unit_from_view if @rah.splitted.include?("views")         #view -> unit
      open_unit_from_hlpr if @rah.splitted.include?("helpers")       #helper -> unit
      open_func_from_ctrl if @rah.splitted.include?("controllers")   #controller -> functional
      @fullpathfile
    end
    #
    #--implementation
    #
    #
    #-- -- unit testing files : unit -> fixture -> functional -> unit ...
    #
    #unit -> fixture
    def open_fixt_file
      z = @file_attr.name.gsub("_test","")
      @fullpathfile = File.join(@rah.base_test,"fixtures","#{z}s.yml")
    end
    #fixture -> functional
    def open_func_file
      z = @file_attr.name
      @fullpathfile = File.join(@rah.base_test,"functional","#{singularize(z)}_controller_test.rb")
      if not FileTest.exist?(@fullpathfile) then
        @fullpathfile = File.join(@rah.base_test,"functional","#{pluralize(z)}_controller_test.rb")
      end
      if not FileTest.exist?(@fullpathfile) then
        LogHelper.inf("File #{@fullpathfile} not found")
      end
    end
    #functional -> unit
    def open_unit_file
      z = @file_attr.name.gsub("_controller","")
      @fullpathfile = File.join(@rah.base_test,"unit","#{z}.rb")
    end
    #-- Ctrl-Shift-!
    #-- -- standard files: model -> unit ; view -> unit ; helper -> unit ; controller -> functional
    #
    #app/models/corbeille.rb -> test/unit/corbeille_test.rb
    def open_unit_from_modl
      @fullpathfile = File.join(@rah.base_test,"unit","#{@file_attr.name}_test.rb")
    end
    #app/views/corbeilles/index.html.erb -> test/unit/corbeille_test.rb
    def open_unit_from_view
      z = @file_attr.dir
      @fullpathfile = File.join(@rah.base_test,"unit","#{z}_test.rb")
    end
    #app/helpers/corbeille_helper.rb -> test/unit/corbeille_test.rb
    def open_unit_from_hlpr
      z = @file_attr.base.gsub('_helper','')
      @fullpathfile = File.join(@rah.base_test,"unit","#{z}.rb")
    end
    #app/controllers/corbeille_controller.rb -> test/functional/corbeille_controller_test.rb
    def open_func_from_ctrl
      z = @file_attr.name
      @fullpathfile = File.join(@rah.base_test,"functional","#{z}_test.rb")
    end

  end

end