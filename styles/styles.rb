# To change this template, choose Tools | Templates
# and open the template in the editor.

include Wx

module RedSun

  class SciteSyntaxColorStyle

    EXCEPT_KEYS = ['$(font.monospace)', '$(colour.string)']

    def initialize(ctrl,config)
      @ctrl = ctrl
      @config = config
    end
    def eolfilled(style_index)
      @ctrl.style_set_eol_filled(style_index, true)
    end
    def back(style_index, value)
      @ctrl.style_set_background(style_index, Colour.new(value))
    end
    def bold(style_index)
      @ctrl.style_set_bold(style_index, true)
    end
    def size(style_index, value)
      @ctrl.style_set_size(style_index, value.to_i)
    end
    def italics(style_index)
      @ctrl.style_set_font(style_index, ITALIC_FONT)
    end
    def font(style_index, value)
      @ctrl.style_set_face_name(style_index, value)
    end
    def fore(style_index, value)
      @ctrl.style_set_foreground(style_index, Colour.new(value))
    end
    def apply_style(key)
      properties = @config[key].split(',')
      style_index = key.split('.')[2].to_i
      properties.each do |prop|
        val = prop.split(':')
        unless EXCEPT_KEYS.include?(val[0])
          if val[1]
            method(val[0]).call(style_index, val[1])
          else
            method(val[0]).call(style_index)
          end
        end
      end
    end

  end

end
