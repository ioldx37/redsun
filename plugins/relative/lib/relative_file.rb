#exemple sur le controller collaborateur:
#  controllers
#    C:\temp\ged_support\www\gedlab\app\controllers\collaborateur_controller.rb
#  models
#    C:\temp\ged_support\www\gedlab\app\models\collaborateur.rb
#  views
#    C:\temp\ged_support\www\gedlab\app\views\collaborateur\index.html.erb
#  helpers
#    C:\temp\ged_support\www\gedlab\app\helpers\collaborateur_helper.rb
#
#  constat: il y a toujours un controller, mais pas toujours de ressource relative.
#    => de quoi on vient ?
#      [res => {controller, model, view, helper, layout, test, application, config}]
#      controller = get_controller_from_resource res
#
#    => ou on veut aller depuis le controller, avec la base du type de fichier courant ?
#      get_relative_resource_from_controller base_current_file_type
#
#
#pour eviter la mini explosion combinatoire 
#

module RedSun

  class RelativeFile

    attr_reader :relative_file_path

    def initialize(frame, file_attr, cfg, pluralize = false)
      @frame = frame
      @cfg = cfg
      @pluralize = pluralize
      @file_attr = file_attr
      @rah = RailsAppHelper.new(file_attr.full_path)
      @relative_file_path = nil
      @fullpathfile = nil
    end

    def get_relative_file
      return unless @rah.is_rails_app?
      @fullpathfile = open_relative_file_from(@rah.get_kind_of?)
    end

    #model -> controller
    def open_ctrl_file
      z = @file_attr.name
      @fullpathfile = File.join(@rah.base_app,"controllers","#{@file_attr.name}_controller.rb")
      if not FileTest.exist?(@fullpathfile) and @pluralize then
        @fullpathfile = File.join(@rah.base_app,"controllers","#{pluralize(z)}_controller.rb")
      end
      @fullpathfile
    end
    
    #controller -> view
    def open_view_file
      z = @file_attr.name.gsub("_controller","")
      @fullpathfile = File.join(@rah.base_app,"views",z,"index.html.erb")
      if not FileTest.exist?(@fullpathfile) then
        mask = File.join(@rah.base_app,"views",z,"*.*")
        file_list = Dir[mask] - ["..","."]
        if not file_list.empty? then
          @fullpathfile = file_list.first
        end
      end
    end
    #view -> helper
    def open_hlpr_file
      z = @rah.splitted[@rah.splitted.size - 2]
      @fullpathfile = File.join(@rah.base_app,"helpers","#{z}_helper.rb")
    end
    #helper -> model
    def open_modl_file
      z = @file_attr.name.gsub("_helper","")
      @fullpathfile = File.join(@rah.base_app,"models","#{z}.#{@file_attr.ext}")
    end
    def open_layt_file
      dlg = FileDialog.new(@frame,"Open file(s)",@file_attr.dir,"",ConfigHelper['file']['erb'],OPEN|MULTIPLE)
      if dlg.show_modal == ID_OK
        files = dlg.get_paths
        if files
          @frame.on_create_drag_n_drop_editor(files)
        end
      end
      @fullpathfile = nil
    end
    def open_unit_modl
      z = @file_attr.name.gsub("_test","")
      @fullpathfile = File.join(@rah.base_app,"models","#{z}.rb")
    end
    #test/fixtures/corbeilles.yml -> app/models/corbeille.rb
    def open_fixt_modl
      z = @file_attr.name
      @fullpathfile = File.join(@rah.base_app,"models","#{singularize(z)}.rb")
    end
    #test/functional/corbeille_controller_test.rb -> app/models/corbeille.rb
    def open_func_ctlr
      z = @file_attr.name.gsub("_controller_test","")
      @fullpathfile = File.join(@rah.base_app,"models","#{z}.rb")
    end
    #ultra lame pluralization strategy ...
    def pluralize(z)
      "#{z}s"
    end
    #ultra lame singularization strategy too,
    def singularize(z)
      z.chop
    end

  end

end