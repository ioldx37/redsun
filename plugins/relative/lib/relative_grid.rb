
module RedSun

  class RelativeGrid < Grid

    def initialize message_book
      super message_book, -1
    end

    def build options = {
        :rows => 10,
        :cols => 3,
        :col_datas => {
          :title => %w[File Status Path],
          :width => [170, 110, 240] } }
      create_grid(options[:rows],options[:cols])
      set_default_cell_background_colour(WHITE)
      set_row_label_alignment(ALIGN_CENTRE, ALIGN_CENTRE)
      set_row_label_size 0
      set_col_label_size 18
      set_col_size 0,0
      options[:cols].times { |i|
        set_col_label_value(i,options[:col_datas][:title][i])
        set_col_size(i,options[:col_datas][:width][i])
      }
      set_col_label_alignment(ALIGN_CENTRE, ALIGN_CENTRE)
    end

  end

end
