module RedSun

  class FileSelectionPopup < ListBox

    def initialize(parent,
        choices = nil,
        pos = DEFAULT_POSITION,
        id = ID_ANY, #Window identifier. A value of -1 indicates a default value
        size = DEFAULT_SIZE,
        style = WANTS_CHARS,
        validator = DEFAULT_VALIDATOR,
        name = "listBox")
      super(parent,id,pos,size,choices,style,validator,name)
    end

  end

end
