
include Wx

module RedSun

  class RedSunFrame < Frame

    attr_reader :message_book, :notebook, :tree, :menu_bar

    def initialize(*args)
      super
      @consts = ConfigHelper['ids']
      @consts.each_with_index { | c, i | RedSunFrame::const_set(c, 2000 + i) }
      @perspectives, @breakpoints, @mru = [], [], []
      ArtProvider::push RedSunArtProvider.new
      self.drop_target = ListFileDropTarget.new self      #[] as drag'n drop files
      @manager = AuiManager.new
      @manager.set_managed_window self
      setup_all
      @manager.update
      LogHelper.inf ConfigHelper['app']['welcome']
    end

    def update_title(title)
    #sync frame title when a file has been open or activated
      self.set_title "#{ConfigHelper['app']['title']} #{ConfigHelper['app']['title separator']} #{title}"
    end

    def update_tree full_path
    #sync tree view when a file has been open or activated
      @tree.set_path full_path
    end

    def update_focus elt
      @focus = elt
    end

    def get_common_path
    #common path is the current file path when a file is already open.
    #otherwise, common path is the current RedSun working directory.
      current_file_path = Dir.getwd
      editor = @notebook.current_editor
      return current_file_path unless editor
      current_file_path = editor.file.full_path if editor.file
    end

    def insert_menu_id str_id
    #a new menu id will be inserted when a plugin needs it
      RedSunFrame::const_set(str_id, @consts.size + 2001)
      @consts << str_id
    end

    def get_menu_bar
      @mb
    end

    def create_scintilla_editor
      file_path = @tree.get_file_path
      file_conf = File.split_all file_path
      return unless file_conf['full_path'].size > 0
      @notebook.add_editor file_conf['full_path']
      @mru << @notebook.current_editor.file.base
      current_editor.set_focus
    end

    def on_create_drag_n_drop_editor(list)
      list.each do |e| 
        @mru << @notebook.add_editor(e).file.base
      end
    end

    def setup_plugin_manager
      plugin_dir = File.join(ConfigHelper['app_dir'],"plugins")
      @pm = RedSun::PluginManager.new self, plugin_dir
      @pm.plug_all
    end

    def switch_pane
      on_switch_pane
    end

    private

    #--
    #--setup section
    #--

    def setup_all
      setup_app_icon
      setup_menu_bar
      setup_status_bar
      setup_panes
      setup_perspectives
      setup_accel_table
    end

    def setup_app_icon
      icon_file = File.join(RedSun::Application.path, 'images', 'redsun.ico')
      icon = Icon.new(icon_file, BITMAP_TYPE_ICO)
      set_icon(icon)
    end

    def setup_menu_bar
      @menu_bar = MenuBar.new
      file_menu = setup_menu 'file_menu'
      edit_menu = setup_menu 'edit_menu'
      nav_menu = setup_menu 'nav_menu'
      run_menu = setup_menu 'run_menu'
      tools_menu = setup_menu 'tools_menu'
      help_menu = setup_menu_help
      #~ setup_perspectives_menu
      @menu_bar.append(file_menu, "&File")
      @menu_bar.append(edit_menu, "&Edit")
      #~ @menu_bar.append(@perspectives_menu, "&Perspectives")
      @menu_bar.append(nav_menu, "&Navigate")
      @menu_bar.append(run_menu, "&Run")
      @menu_bar.append(help_menu, "&Help")
      @menu_bar.append(tools_menu, "&Tools")
      set_menu_bar(@menu_bar)
    end

    def setup_status_bar
      create_status_bar
      sb = get_status_bar
      sb.set_status_text("Ready")
      sb.hide if (ConfigHelper['app']['status'] == 0)
    end

    def setup_menu_help
    #this menu has a parameter, let's say traditional method is convenient.
      menu = Menu.new
      menu.append(ID_ABOUT, "About #{ConfigHelper['app']['title']}...")
      menu
    end

    def setup_menu_help
      help_menu = Menu.new
      help_menu.append(ID_ABOUT, "About #{ConfigHelper['app']['title']}...")
      help_menu
    end

    def setup_menu_perspectives
      @perspectives_menu = Menu.new
      @perspectives_menu.append(ID_CreatePerspective,"Create Perspective")
      @perspectives_menu.append_separator
      begin
        p = File.open(ConfigHelper['file']['perspectives']) { |file| Marshal.load(file) }
        p.each { |e| @manager.load_perspective(e,false) }
        pf, i, rgx = p[0], 1, /name=(\w+)/x
        m = rgx.match(pf)
        while m do
          @perspectives_menu.append(ID_FirstPerspective + i, m[1].capitalize)
          i += 1
          m = rgx.match(m.post_match)
        end
      rescue
      end
      @manager.update
    end

    def set_event_menu id, &method
      evt_menu(id) { yield }
    end

    def on_key_down event
      t = event.get_event_type
      puts "on_key_down with event type : #{t}"
      t = t
    end

    def setup_panes
      setup_workspace
      setup_message_book
      setup_notebook
    end

    def setup_workspace
      pi = AuiPaneInfo.new
      pi.set_name(ConfigHelper['workspace']['name'])
      pi.set_caption(ConfigHelper['workspace']['caption'])
      pi.set_layer(1).set_position(0)
      pi.caption_visible = ConfigHelper['workspace']['caption visible']
      @tree = create_directory_tree_ctrl
      @tree.setup_events
      @manager.add_pane(@tree, pi)
      @workspace_hidden = true
      @message_book_hidden = true
      
      @project_tree = create_project_tree_ctrl
      @project_tree .setup_events
      @manager.add_pane(@project_tree , pi)

      @manager.get_pane(ConfigHelper['workspace']['name']).show
    end

    def setup_message_book
      client_size = get_client_size
      pi = AuiPaneInfo.new
      pi.set_name(ConfigHelper['message book']['name'])
      pi.set_caption(ConfigHelper['message book']['caption'])
      pi.bottom.set_layer(2).set_position(0)
      pi.caption_visible = ConfigHelper['message book']['caption visible']
      @message_book = MessageBook.new(self,ID_ANY,Point.new(client_size.x,client_size.y),Size.new(130, 80))
      @message_book.set_name(ConfigHelper['message book']['name'])
      @manager.add_pane(@message_book, pi)
      @message_book.set_tab_ctrl_height((ConfigHelper['app']['tab heigh']).to_i)
      @manager.add_pane(@message_book, pi)
      setup_message_book_clients
      @manager.get_pane(ConfigHelper['message book']['name']).show
    end

    def setup_message_book_clients
      setup_log_tab
    end

    def setup_log_tab
      @log_window = LogWindow.new(@message_book)
      LogHelper.set_display @log_window
      @message_book.add_book @log_window, 'log'
    end

    def setup_notebook
      pi = AuiPaneInfo.new
      pi.set_name('notebook').center_pane.set_layer(0).set_position(0)
      @notebook = create_special_notebook
      @notebook.add_editor
      tab_ctrl_heigh = (ConfigHelper['app']['tab heigh']).to_i
      @notebook.set_tab_ctrl_height(tab_ctrl_heigh)
      @manager.add_pane(@notebook, pi)
      @manager.get_pane('notebook').show
    end

    def setup_perspectives
      perspective_all = @manager.save_perspective
      @manager.each_pane { | pane | pane.hide unless pane.is_toolbar }
      @manager.get_pane("workspace").hide.left.set_layer(1).set_row(0).set_position(0)
      @manager.get_pane("log").hide.bottom.set_layer(0).set_row(0).set_position(0)
      @manager.get_pane("notebook").show
      perspective_default = @manager.save_perspective
      @perspectives << perspective_default
      @perspectives << perspective_all
    end

    def setup_accel_table
      self.accelerator_table = AcceleratorTable[
        [ACCEL_NORMAL,K_TAB,ID_SNIPPET],
        [ACCEL_SHIFT,K_TAB,ID_SNIPPET_REVERSE],
        #~ [ACCEL_CTRL,K_TAB,ID_Switch_Pane],
        [ACCEL_CTRL,'w',ID_CLOSE]
      ]
    end

    #--
    #--create views section
    #--

    def create_directory_tree_ctrl
      TreeDirectory.new self
    end

    def create_project_tree_ctrl
      TreeProjet.new self
    end

    def create_special_notebook
      client_size = get_client_size
      MainNoteBook.new(self, ID_ANY, Point.new(client_size.x, client_size.y), Size.new(430, 200))
    end

    #--
    #--event handling section
    #--

    def on_erase_background(event)
      event.skip
    end

    def on_size(event)
      event.skip
    end

    def on_notebook_page_close(event)
    #cross button of the tab has been clicked
      ret = on_save
      return event.veto unless ret
    end

    #middle mouse button has been clicked on the tab
    def on_notebook_tab_middle_up(event)
      @notebook.on_close
    end

    def on_new
      @notebook.add_editor
    end

    def on_open
    #select file from open file dialog box
      wildcard = ConfigHelper['file']['wildcard']
      dlg = FileDialog.new(self,"Open file(s)",get_common_path,"",wildcard,OPEN|MULTIPLE)
      if dlg.show_modal == ID_OK
        paths = dlg.get_paths
        on_create_drag_n_drop_editor(paths)
      end
    end

    def on_save
      editor = @notebook.current_editor
      if editor.modified?
        if @notebook.current_editor_untitled?
          on_save_as
        else
          @notebook.on_save
        end
      end
      LogHelper.inf "file #{editor.file.base} saved!"
    end

    def on_save_as
      editor = @notebook.current_editor
      old_name = editor.file.base
      @notebook.on_save_as
      update_title editor.file.path
      LogHelper.inf "file #{old_name} saved as #{editor.file.base}!"
    end

    def on_save_all
      @notebook.on_save_all
    end

    def on_close
      @notebook.on_close
    end

    def on_close_all
      @notebook.on_close_all
    end

    def on_exit
      return if @notebook.on_exit == false
      LogHelper.inf ConfigHelper['app']['bye-bye']
      @pm.unplug_all
      exit
    end

    def on_toggle_workspace_book
      name = ConfigHelper['workspace']['name']
      toggle_panel(name, @workspace_hidden)
      @workspace_hidden = !@workspace_hidden
    end

    def on_toggle_message_book
      name = ConfigHelper['message book']['name']
      toggle_panel(name, @message_book_hidden)
      @message_book_hidden = @message_book_hidden
    end

    def on_about
      message = ConfigHelper['app']['about']
      message << "\nVersion " << ConfigHelper['app']['version']
      dlg = MessageDialog.new(self, message, ConfigHelper['app']['title'], OK)
      dlg.show_modal
    end

    def on_previous_tab
      @notebook.previous_tab unless @focus == @tree
    end

    def on_next_tab
      @notebook.next_tab unless @focus == @tree
    end

    #--
    #--pane section section
    #--

    def on_workspace_pane
      @tree.set_focus
      @focus = @tree
      toggle_workspace
    end

    def on_notebook_pane
      @notebook.current_editor.set_focus
    end

    def on_switch_pane
      #debug
      LogHelper.deb "frame::on_switch_pane"
      w = @manager.get_managed_window
      w = w
    end

    def on_message_pane
      @focus = @message_book
      @message_book.current_tab.set_focus
      toggle_message_book if @message_book_hidden
    end

    #--
    #--run/debug section: methods to be completed
    #--

    def on_run
      editor = @notebook.current_editor
      ext = editor.file.ext
      cmd = ConfigHelper['language'][ext]['command']
      if cmd
        full_file_path = editor.file.full_path
        LogHelper.display_only "running #{editor.file.base} .."
        out = %x[#{cmd} #{full_file_path}]
        LogHelper.display_only "#{out}"
      else
        LogHelper.wrn "no command for #{ext} file type!"
      end
    end

    def on_debug
      full_file_path = @notebook.current_editor.file.full_path
      require 'ruby-debug-ide'
      #      Debugger.start unless Debugger.started?
      Debugger.start unless Debugger.started?
      Debugger.current_context.tracing = true
      Debugger.add_breakpoint full_file_path, 1
      Debugger.debug_load(full_file_path)
      require 'pp'
      pp Debugger.breakpoints.methods
      puts Debugger.infos
    end

    def on_debug_step_over
      Debugger.current_context.step_over 1
      puts Debugger.current_context.inspect
    end

    def on_debug_step_into
      Debugger.current_context.step 1
      puts Debugger.current_context.inspect
    end

    def on_debug_set_breakpoint
      Debugger.current_context.add_breakpoint @notebook.current_editor.file.full_path, 1
      puts Debugger.current_context.inspect
    end

    #--
    #--bookmark section
    #--

    def on_bookmark_add
      @notebook.on_bookmark_add
    end

    def on_bookmark_remove
      @notebook.on_bookmark_remove
    end

    def on_bookmark_next
      @notebook.on_bookmark_next
    end

    def on_bookmark_previous
      @notebook.on_bookmark_previous
    end

    #--
    #--find section
    #--

    def on_find
      @notebook.on_find
    end

    def on_find_selection_down
      @notebook.on_find_selection :next
    end
    
    def on_find_selection_up
      @notebook.on_find_selection :previous
    end
    
    def on_find_selection_again_down
      @notebook.on_find_selection :next
    end
    
    def on_find_selection_again_up
      @notebook.on_find_selection :previous
    end

    def on_find_in_files
       dlg = TextEntryDialog.new(self,
                                 "Find in Files\n",
                                 "Please enter a string",
                                 "def;app/*.rb",
                                 OK | CANCEL)
       return if dlg.show_modal() == ID_CANCEL
       what, where = dlg.get_value.split(';')
       cmd = "grep --directories=recurse --line-number #{what} #{where}"
       out = %x[#{cmd}]
       LogHelper.display_only "find #{what} in #{where}..\n#{out}"
    end

    def build_find_in_files_grid options = {
        :rows => 10,
        :cols => 3,
        :col_datas => {
          :title => %w[File Status Path],
          :width => [170, 110, 240] } }
      grid = Grid.new
      grid.create_grid(options[:rows],options[:cols])
      grid.set_default_cell_background_colour(WHITE)
      grid.set_row_label_alignment(ALIGN_CENTRE, ALIGN_CENTRE)
      grid.set_row_label_size 0
      grid.set_col_label_size 18
      grid.set_col_size 0,0
      options[:cols].times { |i|
        grid.set_col_label_value(i,options[:col_datas][:title][i])
        grid.set_col_size(i,options[:col_datas][:width][i])
      }
      grid.set_col_label_alignment(ALIGN_CENTRE, ALIGN_CENTRE)
    end
    
    def on_replace
      @notebook.on_replace
    end

    #--
    #--misc event section
    #--

    def on_popup_file_list
    #frame::on_popup_file_list -> notebook::open_selected_popup_file -> editor::on_user_list_selection
      @notebook.popup_file_list
    end

   def on_margin_click(evt)
      sci = evt.get_event_object
      line_num = sci.line_from_position(evt.get_position)
      margin = evt.get_margin
      if(margin == 1)
        sci.toggle_fold(line_num)
      end
    end

    def on_preferences
      #    float_pane = @manager.get_pane("settings").float.show
      #    if float_pane.get_floating_position == DEFAULT_POSITION
      #      float_pane.set_floating_position( get_start_position )
      #    end
      #    @manager.update
      puts "on_preferences: to be (re)defined!! (uncomment?)"
    end

    def on_comment
      @notebook.comment
    end

    def on_load_plugin
        puts "on_load_plugin: to be defined!!"
    end

    #--
    #--snippet section
    #--

    def on_snippet
      @notebook.current_editor.on_snippet
    end

    def on_snippet_reverse
      @notebook.current_editor.on_snippet_reverse
    end

    #--
    #--toggle panel section
    #--

    def toggle_panel name, flag
      flag ? @manager.get_pane(name).show : @manager.get_pane(name).hide
      @manager.update
    end

    #--
    #--perspective section
    #--

    #~ def on_create_perspective
      #~ msg = "Create a New Perspective"
      #~ dlg = TextEntryDialog.new(self, msg, "Perspective")
      #~ return unless dlg.show_modal == ID_OK
      #~ @perspectives_menu.append_separator if @perspectives.length.zero?
      #~ @perspectives_menu.append(ID_FirstPerspective + @perspectives.length, dlg.get_value)
      #~ @perspectives << @manager.save_perspective
      #~ File.open('./RedSunPerspectives.txt','w') { |file| Marshal.dump(@perspectives, file) }
    #~ end

    #~ def on_restore_perspective(event)
      #~ perspective = @perspectives[event.get_id - ID_FirstPerspective]
      #~ @manager.load_perspective(perspective)
    #~ end

    def on_create_perspective
      msg = "Create a New Perspective"
      save_perspective_file_name = File.join(RedSun::Application.path,'RedSunPerspectives.txt')
      
      LogHelper.inf "on_create_perspective: #{save_perspective_file_name}"
      
      dlg = TextEntryDialog.new(self, msg, "Perspective")
      return unless dlg.show_modal == ID_OK
      @perspectives_menu.append_separator if @perspectives.length.zero?
      id = get_menu_id_from_key "ID_FirstPerspective"
      @perspectives_menu.append(id + @perspectives.length, dlg.get_value)
      @perspectives << @manager.save_perspective
      save_perspective_file_name = File.join(RedSun::Application.path,'RedSunPerspectives.txt')
      File.open(save_perspective_file_name,'w') { |file| Marshal.dump(@perspectives, file) }
    end

    def on_restore_perspective(event)
      id = get_menu_id_from_key "ID_FirstPerspective"
      perspective = @perspectives[event.get_id - id]
      return unless perspective
      @manager.load_perspective(perspective)
      @tree.set_focus
    end

    def setup_menu menu_name
      menu = Menu.new
      menu_entry = ConfigHelper['shortcuts'][menu_name]
      menu_entry.each do |k,v|
        case k
          when "separator"
            menu.append_separator
          else
            m_title, m_key, m_method = menu_entry[k]
            id = get_menu_id_from_key k
            menu.append(id, "#{m_title}\t#{m_key}")
            eval("set_event_menu(#{id}) { #{m_method} }") if m_method
        end
      end
      menu
    end

    def get_menu_id_from_key name
      id = eval("RedSunFrame::#{name}")
      puts "id is : #{id}"
      id
    end

  end

end
