module RedSun

  class FileAttr

    attr_reader :full_path, :dir, :path, :base, :name, :ext, :icon

    def initialize(full_file_path = nil)
      assign_file_attributes full_file_path
    end

    def renamed? full_file_path
      assign_file_attributes full_file_path
    end

    def rename(full_file_path)
      assign_file_attributes full_file_path
    end

    def set_icon icon
      @icon = icon
    end

    private

    def assign_file_attributes full_file_path   #ex: c:/temp/www/gedlab/app/controllers/hello_controller.rb
      splitted = File.split_all full_file_path
      @is_saved = true
      @icon = nil
      @dir = splitted['dir']                                #controllers
      @path = splitted['path']                              #c:/temp/www/gedlab/app/controllers
      @base = splitted['base']                              #hello_controller.rb
      @name = splitted['name']                              #hello_controller
      @ext = splitted['ext']                                #rb
      @full_path = full_file_path || File.join(@path,@base) #c:/temp/www/gedlab/app/controllers/hello_controller.rb
    end

  end

end
