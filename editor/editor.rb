include Wx

module RedSun

  class Editor < StyledTextCtrl
  
    attr_reader :file

    def initialize(notebook, full_file_path = nil)
      super(notebook,ID_ANY, DEFAULT_POSITION, Size.new(400, 300) )
      @notebook = notebook
      @current_theme = nil
      @content_changed = false
      @file = FileAttr.new full_file_path
      @text_finder = TextFinder.new self
      self.drop_target = ListFileDropTarget.new(notebook.frame,[])
      init
    end

    def init
      setup_fonts
      setup_style
      setup_margins
      setup_indents
      setup_theme
      setup_mouse
      setup_properties
      setup_caret
      setup_events
    end

    def modified?
      get_modify
    end

    def content_changed?
      @content_changed
    end

    #The control was changed from it?s last-saved state
    def on_save_point_left evt
      @content_changed = true
    end

    #The last-saved state was reached in an undo/redo chain
    def on_last_saved_state_reached_in_an_undo_redo evt
      @content_changed = false
      if @stared
        @notebook.refresh_tab_name self
        @stared = false
      end
    end
    
    def on_modified evt
      @content_changed = true
    end

    #char event capture
    def on_char_added evt
      unless @stared
        @notebook.refresh_tab_name self
        @stared = true
      end
      sci = evt.get_event_object
      chr =  evt.get_key
      curr_line = sci.get_current_line
      if(chr == 13)
        if curr_line > 0
          line_ind = sci.get_line_indentation(curr_line - 1)
          if line_ind > 0
            sci.set_line_indentation(curr_line, line_ind)
            sci.goto_pos(sci.position_from_line(curr_line)+line_ind)
          end
        end
      end
    end

    def on_snippet
      line_nbr = get_current_line
      pos = get_current_pos
      snippet = get_line(line_nbr).split.last
      snippet_text = ConfigHelper['snippets'][snippet]
      pos_end = pos
      if snippet_text
        @snippet_mode = true
        pos_start = pos_end - snippet.size
        snippet_text.gsub!("\\n", 13.chr)
        set_selection(pos_start,pos_end)
        replace_selection(snippet_text)
      else
        tab
        @snippet_mode = false
      end
    end

    def on_snippet_reverse
      return tab unless @snippet_mode
    end

    #fold/unfold evt capture
    def on_margin_click(evt)
      sci = evt.get_event_object
      line_num = sci.line_from_position(evt.get_position)
      margin = evt.get_margin
      if(margin == 1)
        sci.toggle_fold(line_num)
      end
    end

    def on_save_point_reached(evt)
    end

    def load
      load_file @file.full_path if @file.full_path
    end

    def save(path = @file.full_path)
      save_file(path)
      @content_changed = false
      @notebook.refresh_tab_name(self)
    end

    def popup_window_list(string_list)
      user_list_show(3, string_list)
    end

    def popup_window_mru_list(string_list)
      user_list_show(3, string_list)
    end

    #--find / replace implementation

    def find_selection direction
      @text_finder.find_selection direction, get_selected_text
    end

    def find_selection_again direction
      find_selection direction
    end

    def show_find_dialog
      @text_finder.show_find_dialog
    end

    def show_find_replace
      @text_finder.show_find_replace
    end

    def handle_find_next
      @text_finder.handle_find_next
    end

    def handle_find_prev
      @text_finder.handle_find_prev
    end

    def on_find evt
      type = evt.get_event_type()
      case type
        when EVT_COMMAND_FIND
          @text_finder.find_command
        when EVT_COMMAND_FIND_NEXT
          @text_finder.find_next_command
        when EVT_COMMAND_FIND_REPLACE
          @text_finder.find_replace_command
        when EVT_COMMAND_FIND_REPLACE_ALL
          @text_finder.find_replace_all_command
      end
    end

    def on_find_close(evt)
      #not implemented yet..
    end

    #-- bookmarks

    def bookmark_add
      line = get_current_line
      marker_add(line,1) #1
    end

    def bookmark_next
      li = marker_next(get_current_line+1,2) #2
      return unless li > 0
      goto_line(li)
    end

    def bookmark_previous
      li = marker_previous(get_current_line-1,2) #2
      return unless li > 0
      goto_line(li)
    end

    def bookmark_remove
      line = get_current_line
      marker_delete(line,1) #1
    end

    def setup_theme
      ext = @file.ext
      ext = ConfigHelper['special']['default'] unless ConfigHelper['language'][ext]
      set_lexer_language(ConfigHelper['language'][ext]['lexer'])
      set_icon
      set_keywords
      style_clear_all
      current_theme = @current_theme
      current_theme ||= ConfigHelper['app']['theme']
      ThemeHelper.apply_theme(self,current_theme)
    end

    def apply_theme name
      @current_theme = name
      ThemeHelper.apply_theme(self,name)
    end

    def comment
      if get_selected_text.size <= 0
        do_comment
      else
        do_comment_all
      end
    end

    def do_comment
      count = 2
      line = get_line(get_current_line)
      comment = ConfigHelper['language']['rb']['comment']
      pos = get_current_pos
      col_nbr = get_column(pos)
      rgx = Regexp.new("^\s*#{comment}(.*)$")
      m = rgx.match(line)
      if m
        sel_start = pos - col_nbr
        set_selection(sel_start,sel_start + line.size)
        line = "#{m[1]}\n"
        replace_selection line
        count = -2
      else
        insert_text(pos - col_nbr,comment)
      end
      goto_pos(pos+count)
    end

    def do_comment_all
      sel_start = get_selection_start
      sel_end = get_selection_end
      comment = ConfigHelper['language']['rb']['comment']
      rlines = get_selected_text
      arr = rlines.split "\n"
      count = (arr.size * comment.size)
      regx = Regexp.new "^\s*#{comment}(.*)$"
      m = regx.match(rlines)
      if m
        arr.collect!{|line| "#{line.gsub!(comment,'')}\n"}
        count -= 1
      else
        arr.collect!{|line| "#{comment}#{line}\n"}
      end
      rlines = arr.join
      replace_selection rlines
      set_selection sel_start, sel_end + count
    end

    private

    def set_icon
      icon = ArtProvider::get_bitmap(get_icon_id, ART_OTHER, Size.new(16,16))
      @file.set_icon(icon)
    end

    def on_user_list_selection(evt)
      @notebook.open_selected_popup_file evt.get_text
    end

    def get_icon_id
      return ConfigHelper['language']['txt']['icon'] unless @file.ext
      ret = ART_REPORT_VIEW
      ext_found = ConfigHelper['language'][@file.ext]
      if ext_found
        ret = ConfigHelper['language'][@file.ext]['icon']
      end
      ret
    end

    def set_keywords
      keywords = ConfigHelper['special']['keywords']
      keywords.each_key {|k| set_key_words(k,keywords[k]) }
    end
  
    def setup_events
      evt_stc_savepointleft(get_id) { | evt | on_save_point_left(evt)}
      evt_stc_savepointreached(get_id) { |evt| on_last_saved_state_reached_in_an_undo_redo(evt) }
      evt_stc_modified(get_id) { | evt | on_modified(evt)}
      evt_stc_charadded(get_id) {|evt| on_char_added(evt)}
      evt_stc_marginclick(get_id) {|evt| on_margin_click(evt)}
      evt_stc_userlistselection(get_id) {|evt| on_user_list_selection(evt)}
      #--find / replace
      evt_find(ID_ANY) {|evt| on_find(evt)}
      evt_find_next(ID_ANY) {|evt| on_find(evt)}
      evt_find_replace(ID_ANY) {|evt| on_find(evt)}
      evt_find_replace_all(ID_ANY) {|evt| on_find(evt)}
      evt_find_close(ID_ANY) {|evt| on_find_close(evt)}
    end

    def setup_style
#      text_back_color = ConfigHelper[][]
#      line_number_back_color = ConfigHelper[][]
#      indent_back_color = ConfigHelper[][]
      style_set_foreground(STC_STYLE_DEFAULT, LIGHT_GREY);
      style_set_background(STC_STYLE_DEFAULT, Colour.new('#130E0A'));
      style_set_foreground(STC_STYLE_LINENUMBER, LIGHT_GREY);
      style_set_background(STC_STYLE_LINENUMBER, Colour.new('#130E0A'));
      style_set_foreground(STC_STYLE_INDENTGUIDE, LIGHT_GREY);
      style_set_background(STC_STYLE_INDENTGUIDE, Colour.new('#130E0A'));
      set_sel_background(true, Colour.new('#695938'))
    end

    def setup_indents
      set_edge_mode(STC_EDGE_LINE)
      set_tab_width(ConfigHelper['special']['tab_width'])
      set_use_tabs(false)
      set_tab_indents(true)
      set_back_space_un_indents(true)
      set_indent(2)
      set_edge_column(0)
      set_margin_sensitive(1,1)
    end

    def setup_fonts
      font = Font.new(10, TELETYPE, NORMAL, NORMAL)
      style_set_font(STC_STYLE_DEFAULT, font);
    end

    def setup_caret
      set_caret_period(0)
      set_caret_line_back_alpha(40)
      set_caret_line_background(RED)
      set_caret_line_visible 1
      set_caret_width(12)
      set_caret_foreground(RED)
    end

    def setup_margins
      setup_line_numbers
      setup_bookmarks
      setup_folds
    end
    
    def setup_line_numbers
      line_num_margin = text_width(STC_STYLE_LINENUMBER,"_99999")
      set_margin_width(0,line_num_margin)
    end

    def setup_bookmarks
      @mask = STC_MARK_DOTDOTDOT  #0x1
      set_margin_width(1,16)
      set_margin_type(1,STC_MARGIN_SYMBOL)
      set_margin_mask(1,@mask)
      marker_define(1,STC_MARK_CIRCLE)
      set_margin_sensitive(1,true)
    end
    
    def setup_folds
      set_margin_type(2,STC_MASK_FOLDERS)
      set_margin_mask(2,31)
      set_margin_sensitive(2,true)
    end

    def setup_properties
      set_property("fold","1")
      set_property("fold.compact", "0")
      set_property("fold.comment", "1")
      set_property("fold.preprocessor", "1")
      set_fold_flags(16)
    end

    def setup_mouse
      set_mouse_dwell_time(120)
    end

  end

end