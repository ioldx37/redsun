
module RedSun

  class ConfirmDialog < Dialog

    attr_reader :editors

    def initialize parent, editors, message = "Select many"
      super(parent, 3210, message)

      @frame = parent
      @editors = editors
      sizer = FlexGridSizer.new(2,1,1)

      lsz = BoxSizer.new VERTICAL
      @lst = ListBox.new(self,ID_ANY,DEFAULT_POSITION, DEFAULT_SIZE, nil, LB_MULTIPLE)
      setup_file_name_list
      lsz.add(@lst)

      bsz = BoxSizer.new VERTICAL
      bt_save = Button.new(self,RedSunFrame::ID_CONFIRM_SAVE,'Save')
      bt_save_all = Button.new(self,RedSunFrame::ID_CONFIRM_SAVE_ALL,'Save All')
      bt_discard = Button.new(self,RedSunFrame::ID_CONFIRM_DISCARD,'Discard')
      bt_discard_all = Button.new(self,RedSunFrame::ID_CONFIRM_DISCARD_ALL,'Discard all')
      bt_cancel = Button.new(self,RedSunFrame::ID_CONFIRM_CANCEL,'Cancel')
      bsz.add(bt_save)
      bsz.add(bt_save_all)
      bsz.add(bt_discard)
      bsz.add(bt_discard_all)
      bsz.add(bt_cancel)

      evt_button(RedSunFrame::ID_CONFIRM_SAVE) { | e | on_save }
      evt_button(RedSunFrame::ID_CONFIRM_SAVE_ALL) { | e | on_save_all }
      evt_button(RedSunFrame::ID_CONFIRM_DISCARD) { | e | on_discard }
      evt_button(RedSunFrame::ID_CONFIRM_DISCARD_ALL) { | e | on_discard_all }
      evt_button(RedSunFrame::ID_CONFIRM_CANCEL) { | e | on_cancel }

      sizer.add_growable_col(0)
      sizer.add(lsz,0, GROW|ALIGN_CENTER_VERTICAL|ALL, 5)
      sizer.add_growable_col(1)
      sizer.add(bsz, 0, GROW|ALIGN_CENTER_VERTICAL|ALL, 5)

      self.sizer = sizer
      sizer.fit(self)
    end

    def on_save
      indexes = @lst.get_selections
      return unless indexes.size > 0
      editor = @editors[indexes.first]
      puts "on_save #{editor.file.base}"
      @frame.notebook.save editor
      @lst.delete(i)
    end

    def on_save_all
      puts "on_save_all"
      indexes = @lst.get_selections
      return unless indexes.size > 0
      indexes.each do |i|
        editor = @editors[i]
        @frame.notebook.save editor
        @lst.delete(i)
      end
      close
    end

    def on_discard
      puts "on_discard !!"
      close
    end

    def on_discard_all
      puts "on_discard_all !!"
      close
    end

    def on_cancel
      puts "on_cancel !!"
      close
    end

    def setup_file_name_list
      short_file_names = @editors.collect { |e| e.file.base }
      @lst.insert_items(short_file_names,0)
    end

  end

end