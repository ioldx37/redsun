module RedSun

  module Component
    attr_accessor :name
    attr_accessor :owner
    def initialize(name)
      @name = name
    end
    def to_s
      @name
    end
    def rename new_name
      @name = new_name
    end
    def children
      @children ||= []
    end
    def add_children *new_children
      new_children.each do |child|
        children.push child
        child.owner = self
      end
    end
    def remove child
      children.delete child
    end
  end

  class ProjectManager
    include Component
    def initialize(name)
      super name
    end
  end

  class Project
    include Component
    def initialize name
      super name
    end
  end

  class ProjectFile
    include Component
    def initialize(name,path)
      @path = path
      super name
    end
  end

  class ProjectHelper
    attr_reader :root
    def initialize root
      @root = root
    end

    def prefix_all prefix,comp = @root
      comp.rename(prefix + comp.name)
      comp.children.each do |child|
        prefix_all(prefix,child)
      end
    end

    def rename name,new_name,root = @root
      comp = find(name)
      found = all.find {|e| e.owner == name}
      found.each {|e| e.owner = new_name} if found
      comp.rename(new_name)
    end

    def all_names root = @root
      root.children.inject([]) do |acc,comp|
        if comp.respond_to?(:name)
          acc << comp.name
          acc.push *all(comp)
        end
      end
    end

    def all root = @root
      root.children.inject([]) do |acc,comp|
        if comp.respond_to?(:name)
          acc << comp
          acc.push *all(comp)
        end
        acc
      end
    end

    def find name
      all.find {|e| e.name == name}
    end
  end

end
