#-----------------------------------------------------
#name: find_in_files.rb
#date: 20130725
#author: ioldx37
#goal: display dialog box to find in files. Called from frame.
#depends: none
#-----------------------------------------------------
module RedSun

  class FindInFilesDialog < Dialog

    def initialize parent, message = "Find in Files.."
      super(parent, 3210, message)
      @frame = parent

      find_what_label = StaticText.new self, ID_ANY, 'what'
      find_what = TextEntry.new self
      find_where_label = StaticText.new self, ID_ANY, 'where'
      find_where = TextEntry.new self

      bt_cancel = Button.new(self,RedSunFrame::ID_FIND_IN_FILES,'Ok')
      bt_ok = Button.new(self,RedSunFrame::ID_CONFIRM_CANCEL,'Cancel')
      bsz = BoxSizer.new VERTICAL
      bsz.add(find_what_label)
      bsz.add(find_what)
      bsz.add(find_where_label)
      bsz.add(find_where)
      bsz.add(bt_ok)
      bsz.add(bt_cancel)

      #~ evt_button(RedSunFrame::ID_FIND_IN_FILES_OK) { | e | on_ok }
      #~ evt_button(RedSunFrame::ID_FIND_IN_FILES_CANCEL) { | e | on_cancel }

    end

    #~ def on_cancel
      #~ close
    #~ end

    #~ def on_ok
      #~ frame.on_find_in_files find_what.text, find_where.text
      
    #~ end

  end

end