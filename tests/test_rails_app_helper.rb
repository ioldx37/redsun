# To change this template, choose Tools | Templates
# and open the template in the editor.

$:.unshift File.join(File.dirname(__FILE__),'..','lib')

require 'test/unit'
require File.join(File.dirname(__FILE__), '../plugins/relative/lib/rails_app_helper')

class RailsAppHelper < Test::Unit::TestCase
  def test_is_rails_app
    pth = 'C:\\temp\\ged_support\\www\\gedlab\\app\\controllers\\collaborateur_controller.rb'
    @rails_app_helper = RedSun::RailsAppHelper.new(pth)
    assert_equal(true, @rails_app_helper.is_rails_app?)
  end
end
