

module RedSun

  class PluginManager

    attr_reader :plugins

    def initialize frame, plugin_dir
      @frame = frame
      @plugin_dir = plugin_dir
      @plugins = {}
    end

    def plug_all
      @available_plugins = ConfigHelper['plugins']['available']
      return unless @available_plugins
      @available_plugins.delete_if { |_,value| !value }
      plugged_in = []
      @available_plugins.each_key do |plugin_name|
        ret = load_plugin(plugin_name)
        ret = register(plugin_name) if ret
        ret = activate(plugin_name) if ret
        plugged_in << plugin_name if ret
        has_menu?(plugin_name) if ret
        has_book_tab?(plugin_name) if ret
      end
      LogHelper.inf "Plugins OK: #{plugged_in.join(', ')}"
    end

    def unplug_all
      @plugins.each do |plugin_name, _|
        deactivate plugin_name
        unplug plugin_name
      end
    end

    private

    def load_plugin plugin_name
      full_file_name = get_plugin_full_path plugin_name
      load_plugin_file plugin_name, full_file_name
    end

    def register plugin_name
      ret = false
      begin
        plugin_class_name = "#{plugin_name.underscore}_plugin".camelize
        @plugins[plugin_name] = plugin_class_name
        ret = true
      rescue
        LogHelper.err "#{ConfigHelper['messages']['plugin register failed']}#{plugin_name}"
      end
      ret
    end

    def activate plugin_name
      ret = false
      if eval("#{@plugins[plugin_name]}.respond_to?('on_activate')")
        begin
          eval("#{@plugins[plugin_name]}.on_activate")
          ret = true
        rescue LoadError
          LogHelper.err "#{ConfigHelper['messages']['plugin activate failed']}#{plugin_name}"
        end
      end
      ret
    end

    def deactivate plugin_name
      begin
        eval("#{@plugins[plugin_name]}.on_deactivate")
      rescue
        LogHelper.err "#{ConfigHelper['messages']['plugin deactivate failed']}#{plugin_name}"
      end
    end

    def unplug plugin_name
      begin
        eval("#{@plugins[plugin_name]}.on_unplug")
        eval("#{@plugins[plugin_name]} = nil")
        ObjectSpace.garbage_collect
        deregister plugin_name
      rescue
        LogHelper.err "#{ConfigHelper['messages']['unplug failed']}#{plugin_name}"
      end
    end

    def deregister plugin_name
      begin
        @plugins.delete plugin_name
      rescue
        LogHelper.err "#{ConfigHelper['messages']['plugin deregister failed']}#{plugin_name}"
      end
    end

    def menu plugin_name
      begin
        eval("#{@plugins[plugin_name]}.on_menu")
      rescue
        LogHelper.err "#{ConfigHelper['messages']['plugin menu setup']}#{plugin_name}"
      end
    end

    def book_tab plugin_name
      begin
        eval("#{@plugins[plugin_name]}.on_book_tab")
      rescue
        LogHelper.err "#{ConfigHelper['messages']['plugin booktab setup']}#{plugin_name}"
      end
    end

    def load_plugin_file plugin_name, full_file_name
      ret = false
      begin
        ret = require full_file_name
      rescue LoadError
        LogHelper.err "#{ConfigHelper['messages']['plugin load failed']}#{plugin_name}"
      end
      ret
    end

    def has_menu? plugin_name
      menu(plugin_name) if eval("#{@plugins[plugin_name]}.respond_to?('on_menu')")
    end

    def has_book_tab? plugin_name
      book_tab(plugin_name) if eval("#{@plugins[plugin_name]}.respond_to?('on_book_tab')")
    end

    def get_plugin_full_path plugin_name
      File.join(ConfigHelper['app_dir'],'plugins',plugin_name,"#{plugin_name.underscore}_plugin.rb")
    end

  end

end
