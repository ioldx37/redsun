class <<File
  
  def norm_join(*args)
    join(args).gsub!("\\",'/')
  end
  
  def split_all(path = nil)
    default = ConfigHelper['special']['untitled']
    splitted = split_base(default)
    file_conf = {'full_path'=>default,
                 'dir' => split_last_dir(Dir.getwd()),
                 'path'=>Dir.getwd(),
                 'base'=>default,
                 'name'=>splitted['name'],
                 'ext' =>splitted['ext']}
    return file_conf unless path
    path.gsub!("\\","/")
    base = File.basename(path)
    splitted = split_base(base)
    file_conf = {'full_path'=>path,
                 'dir' => split_last_dir(path),
                 'path'=>File.dirname(path),
                 'base'=>base,
                 'name'=>splitted['name'],
                 'ext'=>splitted['ext']}
  end

  def split_base(base)
    name, ext = nil, nil
    splitted = /(.*)\.(.*)$/.match(base)
    if splitted
      name = splitted[1]
      ext = splitted[2]
    end
    {'name'=>name, 'ext'=>ext}
  end

  def split_last_dir d
    a = d.split('/')
    (a - [a.last]).last
  end

end
