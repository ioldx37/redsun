=begin
 * *Author* LDx
 * *Copyleft* see WxRuby License
 * *Date* 20130122
 * *Goal* a plugin to handle relative rails files
 * *Usage*
   Please, complete methods as appropriate
 * *Depends* :
   - +dep+ : see plugger module
=end


$: << File.dirname(__FILE__)

require 'lib/relative_file'
require 'lib/relative_grid'
require 'lib/rails_app_helper'
require 'lib/relative_file_config'
require 'lib/relative_file_test'

module RedSun
  
  class RelativePlugin < Plugin

    def self.on_activate
      @relative_files = {
        :files => RelativeFile,
        :tests => RelativeFileTest,
        :config => RelativeFileConfig }
      LogHelper.inf "#{self} Activated"
      @relative_files = {:files=>RelativeFile,:tests=>RelativeFileTest,:config=>RelativeFileConfig}
    end

    def self.on_menu
      create_menu("Relati&ve") do
        add_menu_entry("ID_RELATIVE_OPEN", "Relative &File\tCtrl-!") { on_open_relative_file }
        add_menu_entry("ID_RELATIVE_APP_OPEN", "&Apps Files\tCtrl_Alt-!") { on_open_relative_app_files }
        add_menu_entry("ID_RELATIVE_LAYOUTS_OPEN", "&Layout Files\tAlt-!") { on_open_relative_layout_files }
        add_menu_entry("ID_RELATIVE_CONFIG_OPEN", "&Config Files\tCtrl-:") { on_open_relative_config_files }
        add_menu_entry("ID_RELATIVE_SPECIAL_OPEN", "&Special Files\tCtrl-Shift-:") { on_open_relative_special_files }
      end
    end

    def self.on_book_tab
      mb = frame.message_book
      @grid = RelativeGrid.new mb
      @grid.build
      mb.add_book @grid, 'Relative'
    end

    def self.full_name
      "Relative Plugin"
    end

    def self.description
      "Relative RedSun Plugin"
    end

    def self.author
      "Ldx"
    end

    def self.version
      "1.0.0.0"
    end

    def self.on_open_relative_file
      on_relative_file_impl(:files, ConfigHelper["relative_files"]["mvc"])
      LogHelper.inf "on_open_relative_file"
    end

    def self.on_open_relative_app_files
      LogHelper.inf "on_open_relative_app_files"
    end

    def self.on_open_relative_layout_files
      LogHelper.inf "on_open_relative_layout_files"
    end

    def self.on_open_relative_config_files
      LogHelper.inf "on_open_relative_config_files"
    end

    def self.on_open_relative_special_files
      LogHelper.inf "on_open_relative_special_files"
    end

    private

    def self.on_relative_file_impl relative_file_type, from_config
      notebook = frame.notebook
      editor = notebook.current_editor
      o = @relative_files[relative_file_type].new(self, editor.file, from_config)
      file_path = o.get_relative_file
      if file_path
        notebook.add_editor file_path
      end
    end

  end

end
