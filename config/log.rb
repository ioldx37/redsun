require 'logger'
require 'singleton'
require 'fileutils'
require 'observer'

#usage (among inf, err, wrn, deb, fatal)
# TheLog.set_logger(full_log_path_file)
# TheLog.inf(message)
# TheLog.wrn(message)

module RedSun

  class LogHelper

    include Observable
    private_class_method :new

    def self.instance
      return @@instance if defined? @@instance
      @@instance = new
    end

    def self.set_logger
      log_path = ConfigHelper['file']['log']
      if not FileTest.exist?(log_path)
        begin
          FileUtils.mkdir(log_path)
        end
      end
      log_full_path = File.join(log_path, "#{ConfigHelper['app']['title']}.log")
      @@log = Logger.new log_full_path, 'daily'
      @@log.formatter = proc { |_, datetime, _, msg| get_log_message(datetime, msg) }
    end

    def self.set_display log_window
      @log_window = log_window
    end

    def self.display_only message
      @log_window.trace "[#{Time.now.strftime('%Y%m%d %H:%M:%S')}] #{message}\n"
    end

    def self.fatal msg
      self.log_impl 'fatal', msg
    end

    def self.err msg
      self.log_impl 'error', msg
    end

    def self.wrn msg
      self.log_impl 'warn', msg
    end

    def self.inf msg
      self.log_impl 'info', msg
    end

    def self.deb msg
      self.log_impl 'debug', msg
    end

    private

    def self.get_log_message datetime, msg
      severity_label = ConfigHelper['log']['info']['label']
      "[#{datetime.strftime('%Y%m%d %H:%M:%S')}] #{severity_label} #{msg}\n"
    end

    def self.log_impl type, msg
      @@dt = Time.now
      @@msg = msg
      @@log.method(type).call(msg)
      @log_window.trace "[#{@@dt.strftime('%Y%m%d %H:%M:%S')}] #{ConfigHelper['log'][type]['label']} #{@@msg}\n"
    end

  end

end
