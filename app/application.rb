include Wx

module RedSun

  class RedSunApp < App

    attr_reader :frame

    def on_init
      splash_auto if ConfigHelper['app']['splash']
      @frame = RedSunFrame.new( nil, ID_ANY, ConfigHelper['app']['title'],
        DEFAULT_POSITION,Size.new(ConfigHelper['app']['width'], ConfigHelper['app']['heigh']))
      @frame.set_size(Rect.new( ConfigHelper['app']['top'],
          ConfigHelper['app']['left'],
          ConfigHelper['app']['width'],
          ConfigHelper['app']['heigh']))
      @frame.center(BOTH) if ConfigHelper['app']['center']
      set_top_window @frame
      @frame.show
      @frame.setup_plugin_manager
      return true
    end

    private

    def splash_auto
      splash_image_name = ConfigHelper['app']['splash image']
      ff = File.join(RedSun::Application.path, 'images', splash_image_name)
      splash_bitmap = Bitmap.new(ff, BITMAP_TYPE_JPEG)
      SplashScreen.new(splash_bitmap, SPLASH_CENTRE_ON_SCREEN|SPLASH_TIMEOUT, 3000, nil, -1)
    end

  end

end

