# To change this template, choose Tools | Templates
# and open the template in the editor.

$:.unshift File.join(File.dirname(__FILE__),'..','lib')

require 'test/unit'
require 'theme_manager'

class File
  def norm_join(*args)
    join(args).gsub!("\\",'/')
  end
end

class ThemeManagerTest < Test::Unit::TestCase
  def test_build_path
    path = File.join(File.dirname(__FILE__),'*.yml')
    assert_equal("C:\\temp\\tools\\redsun\\themes/*.yml", path)
  end
  def test_dir_from_path
    path = File.norm_join(File.dirname(__FILE__),'*.yml')
    assert_equal(true, path.size>0)
  end
end
