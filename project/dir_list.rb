module RedSun

  class DirList
  
    require 'find'
  
    attr_reader :tv
  
    def initialize
      @tv = {}
    end

    def get_tree path
      Find.find(path) do |e|
        if FileTest.directory?(e)
          @tv[e] = []
        else
          @tv[File.dirname(e)] << File.basename(e)
        end
      end
    end

  end

end
