=begin
 * *Author* LDx
 * *Copyleft* see wxRuby License
 * *Date* 20121112
 * *Goal* GitPlugin is the GIT plugin as a wrapper of the git ruby gem
 * *Usage*
   Please, complete methods as appropriate
 * *Depends* :
   - +dep+ : see plugger module
=end

$: << File.dirname(__FILE__)

module RedSun

  class GitPlugin < Plugin

    def self.on_activate
      require 'rubygems'
      require 'git'
      require 'lib/git_lib'
      require 'lib/git_grid'
    end

    def self.on_menu
      create_menu("&Git") do
        add_menu_entry("ID_GIT_F5", "Git Status\tCtrl-Shift-f5") { on_git_status }
        add_menu_entry("ID_GIT_F6", "F6\tCtrl-Shift-f6") { on_git_f6 }
        add_menu_entry("ID_GIT_F7", "F7\tCtrl-Shift-f7") { on_git_f7 }
        add_menu_entry("ID_GIT_F8", "F8\tCtrl-Shift-f8") { on_git_f8 }
      end
    end

    def self.on_book_tab
      mb = frame.message_book
      @grid = GitGrid.new mb
      @grid.build
      mb.add_book @grid, 'Git'
    end

    def self.full_name
      "Git Plugin"
    end

    def self.description
      "Git RedSun Plugin"
    end

    def self.author
      "Ldx"
    end

    def self.version
      "1.0.0.0"
    end

    def self.on_git_status
      GitLib.open RedSun::Application.path
      status = GitLib.status
      dd = status[:deleted].sort.collect {|e| [e, "Deleted"]}
      aa = status[:added].sort.collect {|e| [e, "Added"]}
      cc = status[:changed].sort.collect {|e| [e, "Changed"]}
      fl = dd + aa + cc
      nr_to_add = fl.size - @grid.get_number_rows
      @grid.append_rows(nr_to_add) if nr_to_add > 0
      @grid.set_cell_value(0,0,status[:author])
      @grid.set_cell_value(0,1,status[:date].to_s)
      @grid.set_cell_value(0,2,status[:otish])
      fl.each { |e|
        i = fl.index(e) + 1
        @grid.set_cell_value(i,0,e[0])
        @grid.set_cell_value(i,1,e[1])
      }
    end

    def self.on_git_f6
      LogHelper.inf "on_git_f6"
    end

    def self.on_git_f7
      LogHelper.inf "on_git_f7"
    end

    def self.on_git_f8
      LogHelper.inf "on_git_f8"
    end

  end

end

