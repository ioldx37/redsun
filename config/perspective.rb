
require 'singleton'
require 'yaml'


class PerspectiveHelper

  include Singleton
  private_class_method :new

  #pour recuperer l'instance
  def self.instance
    return @@instance if defined? @@instance
    @@instance = new
  end

  def self.load(config_file_name)
    @config_file_name = config_file_name
    #load_config @config_file_name
  end

  def self.save
    #save_config @config_file_name
  end

end


