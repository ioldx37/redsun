=begin
 * *Author* LDx
 * *Copyleft* see MIT License
 * *Date* 20121112
 * *Goal* MyPlugin is the first plugin created as a template plugin file
 * *Usage*
   Please, complete methods as appropriate
 * *Depends* :
   - +dep+ : see plugger module
=end

module RedSun

  class SamplePlugin < Plugin

    def self.on_plug
      LogHelper.inf "Wowow, I am #{self} and I'm plugged from RedSun App itself!!"
    end

    def self.on_activate
      LogHelper.inf "yeeee-haa, I am #{self} and I'm activated. Some statements have to be executed.."
      frame.update_title "Yeeeee-Haa, this is my Title!!"
    end

    def self.on_deactivate
      LogHelper.inf "Thank you to use '#{self}' RedSun plugin!"
    end

    def self.full_name
      "Sample Plugin"
    end

    def self.description
      "a sample RedSun Plugin"
    end

    def self.author
      "Ldx"
    end

    def self.version
      "1.0.0.0"
    end

  end

end
