require 'singleton'
require 'yaml'

module RedSun

  ##
  # La classe ThemeHelper est charg�e de gerer les themes
  #
  class ThemeHelper

    DEFAULT_THEME_NAME = 'default'

    include Singleton
    private_class_method :new

    #init singleton instance one shot for the entire app
    def self.instance
      return @@instance if defined? @@instance
      @@instance = new
    end
    #init de la collection des configs de themes, et selection du theme par defaut
    def self.load
      @theme_collection = {}
      load_all_themes
      select_theme
    end
    #select a theme
    def self.select_theme(theme_name = 'default')
      @theme_collection[theme_name]
    end
    #apply selected theme
    def self.apply_theme(editor, theme_name = 'default')
      return unless @theme_collection[theme_name]  #prevent bad theme name
      sscs = SciteSyntaxColorStyle.new(editor, @theme_collection[theme_name])
      [(1..35),40,41].collect { |e| Array(e) }.flatten.each { |style_code|
        sscs.apply_style("style.ruby.#{style_code}")
      }

      editor.style_set_foreground(STC_STYLE_DEFAULT, LIGHT_GREY)

#      theme = @theme_collection[@current_config_name]
#      a = theme['style.default.32']
#      LogHelper.inf "a = #{a}"

      editor.style_set_background(STC_STYLE_DEFAULT, Colour.new('#130E0A'))
      editor.style_set_foreground(STC_STYLE_LINENUMBER, LIGHT_GREY);
      editor.style_set_background(STC_STYLE_LINENUMBER, Colour.new('#130E0A'))
      editor.style_set_foreground(STC_STYLE_INDENTGUIDE, LIGHT_GREY);
      editor.style_set_background(STC_STYLE_INDENTGUIDE, Colour.new('#382702'))
      editor.set_sel_background(true, Colour.new('#695938'))

    end
    #theme name list
    def self.theme_list
      @theme_collection.keys
    end

    private

    #load all themes
    def self.load_all_themes
      path = File.join(File.dirname(__FILE__).gsub('\\','/'),'..','themes','*.yml')
      all_config_files = Dir[path]
      all_config_files.each do | full_path_file_name |
        load_impl(full_path_file_name)
      end
    end
    #load a single theme
    def self.load_impl(full_path_file_name)
      config = BaseConfig.new
      config.load(full_path_file_name)
      @current_config_name = File.basename(full_path_file_name).split('.')[0]
      @theme_collection[@current_config_name] = config
    end
  
  end

end
