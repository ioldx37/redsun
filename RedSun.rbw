#nom: RedSun.rb
#auteur: LDx
#date:20110422
#objet: IDE pour Ruby et Rails


require 'rubygems' 
require 'wx'

include Wx

module RedSun

  module Application

    def self.start
      @path = File.dirname(__FILE__)
      require_all_project_files
      @app = RedSunApp.new
      ConfigHelper.load(File.join(@path,'config.yml'))
      LogHelper.set_logger
      ThemeHelper.load
      @app.main_loop
    end

    def self.path
      @path
    end

    def self.app
      @app
    end

    private

    def self.require_all_project_files
      [ %w[ project dir_list ],
        %w[ plugins plugin_manager plugin ],
        %w[ styles styles themes provider ],
        %w[ config config file log ],
        %w[ app application frame log_window notebook confirm messagebook tree tree_project find_in_files],
        %w[ editor dnd editor file_attr file_popup text_finder] ].each { | arr |
          arr[1..arr.size].each { | mdi_req_file | require File.join(@path, arr[0], mdi_req_file ) }
        }
    end

  end

end

RedSun::Application.start
