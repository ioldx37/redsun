module RedSun

  class RailsAppPath
    attr_reader :path, :root, :name

    def initialize(path)
      @paths = { :base_path => path, :app_path => nil, :app_root => nil, :app_name => nil }
      @rails_app_detected = false
      @splitted = []
      split_base_path
      rails_detect
    end

    def is_rails_app?
      @rails_app_detected
    end

    def path_to(dir)
      return nil unless @splitted.include?(dir)
      slic = @splitted.slice(0,@splitted.index(dir))
      File.join(@disk_unit,slic)
    end
    
    def split_to(dir)
      return nil unless @splitted.include?(dir)
      @splitted.slice(0,@splitted.index(dir))
    end

    def kind_of? 
      @kind_of = :models if @splitted.include?("models")
      @kind_of = :views if @splitted.include?("views")
      @kind_of = :helpers if @splitted.include?("helpers")
      @kind_of = :controllers if @splitted.include?("controllers")
      @kind_of = :components if @splitted.include?("components")
      @kind_of = :layouts if @splitted.include?("layouts")
      @kind_of
    end

    private

    def split_base_path
      @splitted = @base_path.gsub('\\','/').split('/')
      @disk_unit = @splitted[0]
      @splitted.shift
    end

    def rails_detect
      @rails_app_detected = app_included || config_included || db_included || public_included || test_included
    end

  end

  class RailsAppHelper

    attr_reader :unit, :path, :splitted, :app_root, :app_name, :app_path, :base_path, :kind_of

    def initialize(path)
      @base_path = path
      @rails_app_detected = false
      @splitted, @base_paths = [],{}
      @app_path = nil
      @app_root = nil
      @app_name = nil
      build_path
    end

    def app_included
      ret = is_included?(%w[app controllers models helpers views components])
      return false unless ret
      calc_root_path_and_app_name('app')
      ret
    end

    def config_included
      ret = is_included?(%w[config environments initializers locales])
      return false unless ret
      calc_root_path_and_app_name('config')
      @kind_of = :environments if @splitted.include?("environments")
      @kind_of = :initializers if @splitted.include?("initializers")
      @kind_of = :locales if @splitted.include?("locales")
      ret
    end
    
    def db_included
      ret = is_included?(%w[db migrate])
      return false unless ret
      calc_root_path_and_app_name('db')
      @kind_of = :migrate if @splitted.include?("migrate")
      ret
    end
    
    def public_included
      ret = is_included?(%w[public images javascripts stylesheets])
      calc_root_path_and_app_name('public') if ret
      ret
    end
    
    def test_included
      ret = is_included?(%w[test unit fixtures functional])
      return false unless ret
      calc_root_path_and_app_name('test')
      @kind_of = :unit if @splitted.include?("unit")
      @kind_of = :fixtures if @splitted.include?("fixtures")
      @kind_of = :functional if @splitted.include?("functional")
      ret
    end
    
    def calc_root_path_and_app_name(dir,shift = 1)
      return if @app_root
      i = @splitted.index(dir) - shift
      p = @splitted.slice(0,i)
      @base_paths[:app_root] = File.join(@disk_unit,p)
      calc_app_name(dir,shift)
    end
    
    def calc_app_name(dir,shift)
      return if @app_name
      a = split_to(dir)
      @app_name = a[a.size - shift]
      @app_path = File.join(@app_root,@app_name)
      calc_base_paths
    end
    
    def is_included?(dirs)
      first = dirs.first
      rest = dirs - [first]
      @splitted.include?(first) && @splitted != ([first] + rest)
    end
    
    def calc_base_paths
      @base_paths[:base_app] = File.join(@app_path,'app')
      @base_paths[:base_config] = File.join(@app_path,'config')
      @base_paths[:base_public] = File.join(@app_path,'public')
      @base_paths[:base_test] = File.join(@app_path,'test')
      @base_paths[:base_log] = File.join(@app_path,'log')
      @base_paths[:base_db] = File.join(@app_path,'db','migrate')
    end

  end

end